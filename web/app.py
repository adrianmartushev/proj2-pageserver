from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def hello():
    return "Welcome to my crappy webpage that took me way too long to make.."

@app.route("/404.html")
def pageNotFound():
    return render_template('404.html')

@app.route("/403.html")
def pageError():
    return render_template('403.html')

@app.route("/trivia.html")
def trivia():
    return render_template('trivia.html')


if __name__ == "__main__":
    app.run(host='0.0.0.0')
